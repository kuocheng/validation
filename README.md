<!--Follow the Style Guide when working on this document.
https://docs.gitlab.com/ee/development/documentation/styleguide.html
When done, remove all of this commented-out text, except a commented-out
Troubleshooting section, which, if empty, can be left in place to encourage future use.-->
---
description: This project is to validate the breast deformation captured from a real breast model.
---

# introduction 
This is for my PhD thesis. 
coordinate_transformation_bottom_plane.py: transform the breast model with several corresponding bottom
plane points. 
coordinate_transformation_bottom_plane_vector.py: transform breast model based on a vector on the bottom
plane chosen by user. 
coordinate_transformation_bottom_point.py: transform breast model based on the longest vector calculated
from the bottom plane. 

## Use cases

All python files have its corresponding bat file. 

## Requirements
- Numpy
- Matplot

## Instructions
Look at bat file.
