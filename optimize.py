# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 11:33:42 2020

@author: vr_lab
"""

import manipulate_abaqus_file
import math
from scipy.optimize import minimize
import socket
import pdb

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

def calculate_error(x):
    abaqus.change_elsticity('*Material, name=FAT\n', x[0])
    abaqus.write_output(temp_inp_file)
    conn, addr = SOCKET.accept()
    with conn:
        print('Connected by', addr) 
 
        data = conn.recv(1024).decode('utf-8')
        if not data:
            return
        
        print('error ' + data)
        print ('fat ' + str(x[0]))
        conn.sendall(('c').encode('utf-8'))
        return float(data)
    return 1
       
def server():
    minimize(calculate_error, fat_elasticity, bounds=[(0.0001,1)], method='L-BFGS-B', options={'disp': True})    
    SOCKET.close()
    
if __name__ == "__main__":
    inp_file = 'data/trial/Skin_Layer_Modified_Reference_x_y_0.inp'
    temp_inp_file = 'data/trial/temp.inp'
    angle = 40
    fat_elasticity = 0.007  
    x = math.cos(math.radians(angle))
    z = math.cos(math.radians(angle)) 
    y = math.sin(math.radians(angle))     
    abaqus = manipulate_abaqus_file.ReadAbaqusInput(inp_file) 
    
    direction = [x, y, 0] 
    gravity_magnitude = 9810
    abaqus.change_gravity('** Name: GRAVITY-1   Type: Gravity\n', direction, gravity_magnitude)
  #  abaqus.write_output(temp_inp_file)
  
    SOCKET = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    SOCKET.bind((HOST, PORT))
    SOCKET.listen()
   
    server()
    SOCKET.close()