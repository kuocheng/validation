# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 09:27:16 2020

@author: vr_lab
"""
import argparse
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import meshio
import utility

import pdb

# move breast to origin based on base plane
# args:
# base_plane_reference: pick 3 points from the base plane, 3x3 numpy array
# returns:
# 
def find_center(base_plane_reference):
    points = []
    global vertices
    global base_points
    vertices = mesh.points
    for vertex in vertices:
        if utility.point_on_same_plane(base_plane_reference, vertex):
            points.append(vertex)
    points_array = np.array(points)
   # plot(points_array)
    center = utility.find_center(points_array)
    base_points = points_array
    return center

def find_scale(base_dim):
    largest_distance = 0;
    point1 = vertices[0]
    point2 =  vertices[0]
    i = 0
    for vertex1 in base_points:
        print(i)
        i += 1
        for vertex2 in base_points:
            length = np.linalg.norm(vertex1 - vertex2)
            if length > largest_distance:
                largest_distance = length
                point1 = vertex1
                point2 = vertex2
   
    return  base_dim / largest_distance, point1, point2
    
def plot(vertices):
    ax = plt.axes(projection='3d')
    ax.scatter3D(vertices[:, 0], vertices[:, 1], vertices[:, 2], c=vertices[:, 2], cmap='Greens');
    
    plt.show()
    pdb.set_trace()

# find the rotation matrix to align vec1 to vec2
def find_rotation_matrix(vec1, vec2):
    vec1 = vec1 / np.linalg.norm(vec1)
    vec2 = vec2 / np.linalg.norm(vec2)
    v = np.cross(vec1, vec2)
    s = np.linalg.norm(v)
    c = np.dot(vec1, vec2)
    skew_cross = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    
   # https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
    R = np.identity(3) + skew_cross + np.dot(skew_cross, skew_cross) / (1 + c) 
    return R
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", help="what file do you want to process")
    parser.add_argument("--plane_ref", help="enter the reference plane in numpy array")
    parser.add_argument("--output_file", help="enter the ourput file name")
    args = parser.parse_args()
    
    breast_file = args.input_file 
    output_file = args.output_file
    plane_reference = eval(args.plane_ref)  
    vertices = np.array([0, 0, 0])
    base_points = np.array([0, 0, 0])
    mesh = meshio.read(breast_file)
    center = find_center(plane_reference)
    
    vertices = utility.move_to_origin(center, vertices)  
    
    pdb.set_trace()
    
    base_dim = 110
    scale, farthest_point1, farthest_point2 = find_scale(base_dim)  #modelCleanUp_EQw0BDVAh1： scale: 91.0378, farthest_point1: [-0.698, 0.682, 5.241], farthest_point2: [0.4938, 0.7015, 5.046]
                                                                    #modelCleanUp_dC8PhoAxpu.stl. scale:  28.8105, farthest_point1=np.array([0.622,3.103,7.644]), farthest_point2=np.array([1.528, -0.592,7.957]) 
    vertices *= scale
    rotation = find_rotation_matrix(farthest_point2 - farthest_point1, [1, 0, 0])
    vertices = np.transpose(np.dot(rotation, np.transpose(vertices)))
    meshio.write_points_cells(output_file, vertices, mesh.cells)
