# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 20:06:49 2020

@author: vr_lab
"""

import socket

HOST = '127.0.0.1'  # The server's hostname or IP address
PORT = 65432        # The port used by the server

def client():
  with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
      s.connect((HOST, PORT))
      while True: 
          s.sendall(('3').encode('utf-8'))
          data = s.recv(1024).decode('utf-8') 
          print(data)

if __name__ == "__main__":
    client()