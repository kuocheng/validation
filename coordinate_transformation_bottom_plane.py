# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 09:27:16 2020
    PATH=$PATH:/F/Program/Anaconda (in order to run in windows)
@author: vr_lab
"""
import argparse
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import meshio
import random
import utility

import pdb

def find_center(vertices, base_plane, threshold):
    points = []
    for vertex in vertices:
        if utility.point_on_same_plane(base_plane, vertex, threshold):
            points.append(vertex)
    points_array = np.array(points)
    
    plot(points_array)
    center = utility.find_center(points_array)
    return center

def find_scale(base_dim, point1, point2):  
    length = np.linalg.norm(point2 - point1)
    return  base_dim / length
    
def plot(vertices):
    ax = plt.axes(projection='3d')
    ax.scatter3D(vertices[:, 0], vertices[:, 1], vertices[:, 2], c=vertices[:, 2], cmap='Greens');
    
    plt.show()

# find the rotation matrix to align vec1 to vec2
def find_rotation_matrix(obj_points, ref_points):
    u, s, v = np.linalg.svd(np.dot(obj_points, np.transpose(ref_points)))   
    R = np.dot(v, np.transpose(u))
    if np.linalg.det(R) < 0:
        u, s, v = np.linalg.svd(R)
        v[:,2] *= (-1)
        R = np.dot(v, np.transpose(u))
    return R

# Input: expects 3xN matrix of points
# Returns R,t
# R = 3x3 rotation matrix
# t = 3x1 column vector
def rigid_transform_3D(A, B):
    assert len(A) == len(B)

    num_rows, num_cols = A.shape;

    if num_rows != 3:
        raise Exception("matrix A is not 3xN, it is {}x{}".format(num_rows, num_cols))

    [num_rows, num_cols] = B.shape;
    if num_rows != 3:
        raise Exception("matrix B is not 3xN, it is {}x{}".format(num_rows, num_cols))

    # find mean column wise
    centroid_A = np.mean(A, axis=1)
    centroid_B = np.mean(B, axis=1)

    # ensure centroids are 3x1 (necessary when A or B are 
    # numpy arrays instead of numpy matrices)
    centroid_A = centroid_A.reshape(-1, 1)
    centroid_B = centroid_B.reshape(-1, 1)

    # subtract mean
    Am = A - np.tile(centroid_A, (1, num_cols))
    Bm = B - np.tile(centroid_B, (1, num_cols))

    H = Am * np.transpose(Bm)

    # sanity check
    #if linalg.matrix_rank(H) < 3:
    #    raise ValueError("rank of H = {}, expecting 3".format(linalg.matrix_rank(H)))

    # find rotation
    U, S, Vt = np.linalg.svd(H)
    R = np.dot(Vt.T, U.T)

    # special reflection case
    if np.linalg.det(R) < 0:
        print("det(R) < R, reflection detected!, correcting for it ...\n");
        Vt[2,:] *= -1
        R = np.dot(Vt.T, U.T)
    t = -np.dot(R, centroid_A) + centroid_B

    return R, t
    



if __name__ == '__main__':
    parser = argparse.ArgumentParser()
   
    parser.add_argument("--reference_file", help="what file do you want to use as coordinate reference")
    parser.add_argument("--object_file", help="what object file do you want to rotate")
    parser.add_argument("--reference_plane_point", help="pick 3 points from reference plane")
    parser.add_argument("--object_plane_point", help="pick 3 points from object plane")
    parser.add_argument("--output_obj_file", help="enter the output_obj_file file name")
    parser.add_argument("--output_ref_file", help="enter the output_ref_file file name")
    args = parser.parse_args()
    
    reference_file = args.reference_file 
    object_file = args.object_file
    output_obj_file = args.output_obj_file
    reference_plane_points = eval(args.reference_plane_point)  
    object_plane_points = eval(args.object_plane_point) 
    output_ref_file = args.output_ref_file
  #  reference_plane_points = np.array([[-2.521837, -4.043342, 8.899975], [-4.701437, -0.177704, 8.301741], [-2.192909, 2.880540, 9.012056]])
    reference_plane_points = reference_plane_points.T
  #  object_plane_points = np.array([[-2.680368, -4.177680, 7.895990], [-4.827431, -0.701787, 6.670202], [-2.491574, 2.473680, 7.092040]])
    object_plane_points = object_plane_points.T
    
    obj_mesh = meshio.read(object_file)
    ref_mesh = meshio.read(reference_file)
    obj_vertices = obj_mesh.points
    ref_vertices = ref_mesh.points
 
    base_dim = 110
    obj_scale = find_scale(base_dim, object_plane_points[:, 0], object_plane_points[:, 2])  #modelCleanUp_EQw0BDVAh1： scale: 91.0378, farthest_point1: [-0.698, 0.682, 5.241], farthest_point2: [0.4938, 0.7015, 5.046]
                                                                                        #modelCleanUp_dC8PhoAxpu.stl. scale:  28.8105, farthest_point1=np.array([0.622,3.103,7.644]), farthest_point2=np.array([1.528, -0.592,7.957]) 
    ref_scale = find_scale(base_dim, reference_plane_points[:, 0], reference_plane_points[:, 2])
    
    obj_vertices *= obj_scale
    ref_vertices *= ref_scale
    reference_plane_points *= ref_scale
    object_plane_points *= obj_scale
    
    # Random rotation and translation， when two objects are close, it will not generate correct rotation. Need to move a little further in the beginning
    R = np.mat(np.random.rand(3,3))
    t = np.mat(np.random.rand(3,1))
    
    # make R a proper rotation matrix, force orthonormal
    U, S, Vt = np.linalg.svd(R)
    R = U*Vt
    
    # remove reflection
    if np.linalg.det(R) < 0:
       Vt[2,:] *= -1
       R = U*Vt
    
    # number of points
    n = object_plane_points.shape[1]

    object_plane_points = R * object_plane_points + np.tile(t, (1, n))
    obj_vertices = obj_vertices.T
    obj_vertices = R * obj_vertices + np.tile(t, (1, obj_vertices.shape[1]))
    
    rotation, trans = rigid_transform_3D(object_plane_points, reference_plane_points)  # np.transpose(np.dot(rotation, np.transpose(object_plane_points)) + np.tile(trans, (1, object_plane_points.shape[0]))) 
    
    pdb.set_trace()
    object_plane_points = (rotation * object_plane_points) + np.tile(trans, (1, n))  
    obj_vertices = np.dot(rotation, obj_vertices) + np.tile(trans, (1, obj_vertices.shape[1])) 
    obj_vertices = obj_vertices.T
    
    
    ref_center = find_center(ref_vertices, reference_plane_points.T, 20000)
    
    obj_vertices = utility.move_to_origin(ref_center, obj_vertices)  
    ref_vertices = utility.move_to_origin(ref_center, ref_vertices) 

    
    meshio.write_points_cells(output_obj_file, obj_vertices, obj_mesh.cells)
    meshio.write_points_cells(output_ref_file, ref_vertices, ref_mesh.cells)
    
    
# python rotate_to_reference.py --reference_file=data/mizkhdlcsM/model_cleanUp.obj --object_file=data/LqVn7ArFp8/model_cleanUp.obj --output_file=data/LqVn7ArFp8/model_scaled.obj