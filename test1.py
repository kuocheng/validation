# -*- coding: utf-8 -*-
"""
Created on Tue Sep 22 20:03:46 2020

@author: vr_lab
"""

import socket

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

def server():
    with socket.socket(socket.AF_INET, socket.SOCK_STREAM) as s:
        s.bind((HOST, PORT))
        s.listen()
       
        conn, addr = s.accept()
        with conn:
            print('Connected by', addr)
            while True:
                data = conn.recv(1024).decode('utf-8')
                if not data:
                    break
                conn.sendall((data).encode('utf-8'))
                print(data)
if __name__ == "__main__":
    server()