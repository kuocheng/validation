# -*- coding: utf-8 -*-
"""
Created on Sun Sep 20 08:43:47 2020

@author: vr_lab
"""

import generate_scenario
import manipulate_abaqus_file
import math
import numpy as np
import odbAccess
from odbAccess import openOdb
import os
import pdb
import readODB
import socket

HOST = '127.0.0.1'  # Standard loopback interface address (localhost)
PORT = 65432        # Port to listen on (non-privileged ports are > 1023)

def find_marker_index(triangle_indices, tet_points, marker_point):
    distance = float('inf')
    marker_index = 0
    for index in triangle_indices:
        if np.linalg.norm(tet_points[index] - marker_point) < distance:
            distance = np.linalg.norm(tet_points[index] - marker_point)
            marker_index = index 
    return marker_index

def read_lastframe(file_name, step, part):
    odb = openOdb(file_name)
    frame_num = -1
    odb_reader = readODB.ReadOdb(file_name)
    args = [odb, step, part, frame_num]
    disps = odb_reader.read_frame(args) 
    odb.close()
    return disps

def client(odbfile):   
    msg = ''
    while msg != 'q':
        s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        s.connect((HOST, PORT))
      
        os.system('abaqus j=' + odbfile.split('.')[0] + ' interactive cpus=8 input=' + temp_inp_file + '  double ' + 'ask_delete=OFF')
        disps = read_lastframe(odbfile, step, part) 
        if os.path.exists('temp_result.lck'):
            os.remove('temp_result.lck')
        if os.path.exists('temp_result.odb'):
            os.remove('temp_result.odb')
        error = 0
        for counter, index in enumerate(marker_indices):
            error += np.linalg.norm(disps[index] - ground_truth[counter, :])
        s.sendall(str(error).encode('utf-8'))
        msg = s.recv(1024).decode('utf-8')
        s.close()
    
if __name__ == "__main__":
    surface_file = 'data/trial/Skin_Layer_Simplified.face'
    ele_file = 'data/trial/Skin_Layer_Simplified.ele'
    node_file = 'data/trial/Skin_Layer_Simplified.node'   
    temp_odb_file = 'temp_result.odb'
    temp_inp_file = 'data/trial/temp.inp'
    part = 'PART-1-1'
    step = 'Step-1'
    ground_truth = np.array([[0.700782, -0.00824319, -0.217146], [0.455423, -0.10018, -0.157274]])   # take from Skin_Layer_Modified_Reference_x_y_40 when glan:fat = 10:90
   
    breast_info = generate_scenario.GetInfoFromBreast(ele_file, node_file)
    triangles = breast_info.read_face_indices(surface_file)
    coordinates = breast_info.read_coordinates(ele_file, node_file)
    marker_points = np.array([[61, 76, 9], [61, 77, 9]])  # x, y, z value
    marker_indices = []
    for counter, point in enumerate(marker_points):
        marker_indices.append(find_marker_index(triangles, coordinates, point))
    client(temp_odb_file)  