# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 09:06:25 2020

@author: vr_lab
"""

import meshio
import numpy as np
import pdb
  
def point_on_same_plane(base_points, check_point, threshold):
    if not isinstance(base_points, np.ndarray) or base_points.shape != (3, 3):
         raise ValueError("Passed array is not of the right shape") 
    
    point_matrix = np.append(base_points, np.array([check_point]), axis=0)
    point_matrix = np.append(point_matrix, [[1], [1], [1], [1]], axis=1)
    point_matrix = point_matrix.transpose()
    determinant = abs(np.linalg.det(point_matrix))
  #  print(determinant)
  #  pdb.set_trace()
    if determinant < threshold:        
        return True
    else:
        return False
    
def find_center(points):
    return np.mean(points, axis=0)

def move_to_origin(center, points):
    return points - center