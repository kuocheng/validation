# -*- coding: utf-8 -*-
"""
Created on Sun Aug 23 09:27:16 2020

@author: vr_lab
"""
import argparse
from mpl_toolkits import mplot3d
import numpy as np
import matplotlib.pyplot as plt
import meshio
import utility

import pdb

def find_scale(base_dim, point1, point2):  
    length = np.linalg.norm(point2 - point1)
    return  base_dim / length
    
def plot(vertices):
    ax = plt.axes(projection='3d')
    ax.scatter3D(vertices[:, 0], vertices[:, 1], vertices[:, 2], c=vertices[:, 2], cmap='Greens');
    
    plt.show()
    pdb.set_trace()

# find the rotation matrix to align vec1 to vec2
def find_rotation_matrix(vec1, vec2):
    vec1 = vec1 / np.linalg.norm(vec1)
    vec2 = vec2 / np.linalg.norm(vec2)
    v = np.cross(vec1, vec2)
    s = np.linalg.norm(v)
    c = np.dot(vec1, vec2)
    skew_cross = np.array([[0, -v[2], v[1]], [v[2], 0, -v[0]], [-v[1], v[0], 0]])
    
   # https://math.stackexchange.com/questions/180418/calculate-rotation-matrix-to-align-vector-a-to-vector-b-in-3d
    R = np.identity(3) + skew_cross + np.dot(skew_cross, skew_cross) / (1 + c) 
    return R
    
if __name__ == '__main__':
    parser = argparse.ArgumentParser()
    parser.add_argument("--input_file", help="what file do you want to process")
    parser.add_argument("--farthest_point", help="enter the farthese point in the reference plane")
    parser.add_argument("--output_file", help="enter the ourput file name")
    args = parser.parse_args()
    
    breast_file = args.input_file 
    output_file = args.output_file
    farthest_point = eval(args.farthest_point)  
    base_points = np.array([0, 0, 0])
    mesh = meshio.read(breast_file)
    vertices = mesh.points
    center = (farthest_point[0] + farthest_point[1]) / 2
    
    vertices = utility.move_to_origin(center, vertices)  
    
    pdb.set_trace()
    
    base_dim = 110
    farthest_point1 = farthest_point[0]
    farthest_point2 = farthest_point[1]
    scale = find_scale(base_dim, farthest_point1, farthest_point2)  #modelCleanUp_EQw0BDVAh1： scale: 91.0378, farthest_point1: [-0.698, 0.682, 5.241], farthest_point2: [0.4938, 0.7015, 5.046]
                                                                    #modelCleanUp_dC8PhoAxpu.stl. scale:  28.8105, farthest_point1=np.array([0.622,3.103,7.644]), farthest_point2=np.array([1.528, -0.592,7.957]) 
    vertices *= scale
    
    rotation = find_rotation_matrix(farthest_point2 - farthest_point1, [1, 0, 0])
    vertices = np.transpose(np.dot(rotation, np.transpose(vertices)))
    meshio.write_points_cells(output_file, vertices, mesh.cells)
